<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function login_success()
    {
        return view('login_success',['user'=>auth::user()->email]);
    }

    public function get_login()
    {
        return view('login');
    }

    public function post_login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect('/');
        }
        else{
            echo "Login Fail";
        }
    }

    public function get_register()
    {
        return view('register');
    }

    public function post_register(Request $request)
    {
        $data=[
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'name'=>$request->name
        ];
        $n=DB::table('users')->insert($data);
        if($n==1)
        {
            return redirect('/');
        }  
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
