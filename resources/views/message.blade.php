<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Message</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{asset('')}}">
  </head>
  <body>
    <div class="panel panel-primary" id="chat_box" style="width:60%;padding:20px;margin:10px auto">
      <div id="message_content">
        @foreach ($messages as $message)
            <div class="panel-heading">
                <i class="fa fa-user" aria-hidden="true"></i> 
                <p><span style="font-weight:bold">{{ $message->user_id }}:</span> <span class="message">{{ $message->message }}</span></p>
            </div>
        @endforeach
      </div>
        
      <div style="border:1px solid black;padding:10px">
        @csrf
        <div class="form-group">
            <label for="message">Message</label>
            <input type="text" class="form-control" id="message" name="message" placeholder="Type your message" required>
        </div>
    
        <button type="submit" id="btn_sent" class="btn btn-primary">Submit</button>
    </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
      $("#btn_sent").click(function(){
          var message=$("#message").val();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url:'{{URL('/')}}',
                data:{_token: '<?php echo csrf_token() ?>',message:message},
                success: function(data){
                  alert(data);
                },
                error: function(xhr,status,error){
                    alert(error);
                }

            });
        });
    </script>
    <script src="{{mix('js/app.js')}}"></script>
  </body>
</html>