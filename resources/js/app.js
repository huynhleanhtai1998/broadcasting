require('./bootstrap');

Echo.private('message')
    .listen('MessageSentEvent', (e) => {
        var message_tag = `<div class="panel-heading">
                                <i class="fa fa-user" aria-hidden="true"></i> 
                                <p><span style="font-weight:bold">` + e.user.name + `:</span> <span class="message">` +
                                e.message.message + `</span></p>
                            </div>`
        document.getElementById("message_content").innerHTML +=  message_tag;
           
        console.log(e);
});
