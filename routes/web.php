<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'LoginController@get_login')->name('login');
Route::post('/login', 'LoginController@post_login');

Route::get('/register', 'LoginController@get_register');
Route::post('/register', 'LoginController@post_register');

Route::get('/logout', 'LoginController@logout')->middleware('auth');

Route::get('/', 'MessageController@index')->middleware('auth');
Route::post('/', 'MessageController@store')->middleware('auth');
